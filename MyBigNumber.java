public class MyBigNumber {
    static String sum(String num1, String num2){
        StringBuilder sum = new StringBuilder();

        int max = Math.max(num1.length(), num2.length());

        //Add "0" into smaller number
        if(max > num2.length()){
            StringBuilder num2Builder = new StringBuilder(num2);
            for (int i = max - num2.length(); i > 0; i--) {
                num2Builder.insert(0, "0");
            }
            num2 = num2Builder.toString();
        }

        else if(max > num1.length()){
            StringBuilder num1Builder = new StringBuilder(num1);
            for (int i = max - num1.length(); i > 0; i--) {
                num1Builder.insert(0, "0");
            }
            num1 = num1Builder.toString();
        }

        int temp = 0;
        for (int i = max-1; i >= 0; i--) {
            int sumOf2Chars;
            sumOf2Chars = (num1.charAt(i) - 48) + (num2.charAt(i) - 48) + temp; //-48 to convert from char to int in ASCII
            System.out.println("Bước "+(max - i) + ", lấy " + num1.charAt(i) + " cộng với " + num2.charAt(i) + " ,cộng với số nhớ là " + temp + ", được " + sumOf2Chars);
            sum.append(sumOf2Chars % 10);
            temp = sumOf2Chars / 10;
            System.out.println("Thêm " + sumOf2Chars % 10 + " vào tổng, số nhớ là " + temp + "\n");
        }
        if (temp == 1) {
            sum.append(1);
        }
        StringBuilder sumStringBuilder = new StringBuilder(sum);
        return sumStringBuilder.reverse().toString();
    }
}
